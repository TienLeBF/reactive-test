package com.example.demo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
@RequiredArgsConstructor
public class RichMenuV2Controller {
    @Autowired
    private RichMenuService richMenuService;
    @Autowired
    private StudentRepository studentRepository;

    @PostMapping(value = "/one")
    public Mono<Void> create(@RequestParam String name, @RequestParam String age) {
//        String name = "test-name-";
//        String age = "test-age-";
        log.info("CREATE ONE Thread name: {} - create name:{}, age: {}", Thread.currentThread().getName(), name, age);
        return richMenuService.create(name, age);
    }

    @PostMapping(value = "/all")
    public Mono<Void> createAll(@RequestParam String name, @RequestParam String age)
            throws IOException {
        log.info("CREATE ALL Thread name: {} - create name:{}, age: {}", Thread.currentThread().getName(), name, age);
        try {
            CloseableHttpClient httpClient = HttpClientBuilder.create()
                                                              .build();
            System.out.println("name: " + name + " age: " + age);
            for (int i = 0; i < 1000; i++) {

                HttpPost httpPost = new HttpPost("http://localhost:8080/one?name=" + name + i + "&age=" + age + i);
                ArrayList<NameValuePair> postParameters = new ArrayList<>();
                HttpResponse response = httpClient.execute(httpPost);
                System.out.println("I = " + i + " Now: " + LocalDateTime.now() + " \nRequest: " + httpPost.toString() + " \nResponse: " + response.toString());
                if (i > 950) {
                    Thread.sleep(5000);
                }
            }
            httpClient.close();
        } catch (Exception e) {
            System.out.println("Loi cmnr ban: " + e.getMessage());
        }
        return null;
    }

    @GetMapping("/exists")
    public Map<Integer, Flux<Student>> exists() {
        //List<Student> students = new ArrayList<>();
        Map<Integer, Flux<Student>> result = new HashMap();
        for (int i = 0; i < 101; i++) {
            Flux<Student> student = studentRepository.findByName("test-name-" + i);
            result.put(i, student);
        }
        return result;
    }

    @PostMapping("/{code}/link")
    public Mono<Void> link() {
       return null;
    }
}
