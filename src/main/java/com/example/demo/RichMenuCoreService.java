package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RichMenuCoreService {
    @Autowired
    private StudentRepository mappingInfoRepository;


    public Mono<Void> create(String name, String age) {
        log.info("SAVE Thread name: {} - create name:{}, age: {}", Thread.currentThread().getName(), name, age);
        return mappingInfoRepository.findByName(name)
                                    .switchIfEmpty(Mono.defer(() -> mappingInfoRepository.save(new Student(name, age))))
                                    .then();
    }


}
