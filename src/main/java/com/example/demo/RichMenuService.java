package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
public class RichMenuService {
    @Autowired
    private RichMenuCoreService RichMenuCoreService;

    public Mono<Void> create(String name, String age) {
        return RichMenuCoreService.create(name, age);
    }
}

